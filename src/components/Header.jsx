import { Button, List, Menu, Typography } from "antd";
import { useRouter } from "next/router";
import React from "react";
import styles from "./index.module.scss";

function Header() {
  const router = useRouter();

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        width: "1048px",
        marginLeft: "auto",
        marginRight: "auto",
        padding: "0px 24px",
      }}
    >
      <List>
        <div
          style={{
            height: "64px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div style={{ display: "flex", marginRight: "100px" }}>
            <List.Item
              className={styles.listitem}
              onClick={() => router.push("/")}
            >
              <Typography
                style={{
                  fontSize: "1.5rem",
                  fontWeight: "bold",
                  color: "black",
                }}
              >
                LOGO
              </Typography>
            </List.Item>
          </div>
          <div
            style={{
              display: "flex",
              fontSize: "14px",
            }}
          >
            <List.Item
              onClick={() => router.push("/")}
              className={styles.listitem}
            >
              <Typography>Features</Typography>
            </List.Item>
            <List.Item
              onClick={() => router.push("/templates")}
              className={styles.listitem}
            >
              <Typography>Templates</Typography>
            </List.Item>
            <List.Item
              onClick={() => router.push("/integrations")}
              className={styles.listitem}
            >
              <Typography>Integrations</Typography>
            </List.Item>
            <List.Item
              onClick={() => router.push("/customers")}
              className={styles.listitem}
            >
              <Typography>Customers</Typography>
            </List.Item>
            <List.Item
              onClick={() => router.push("/pricing")}
              className={styles.listitem}
            >
              <Typography>Pricing</Typography>
            </List.Item>
          </div>
          <div
            style={{
              display: "flex",
              fontSize: "14px",
              marginLeft: "50px",
              color: "#666666",
            }}
          >
            <List.Item
              onClick={() => router.push("/contact")}
              className={styles.listitem}
            >
              <Typography>Contact</Typography>
            </List.Item>
            <List.Item
              onClick={() => router.push("/login")}
              className={styles.listitem}
            >
              <Typography>Login</Typography>
            </List.Item>
            <List.Item
              onClick={() => router.push("/signup")}
              className={styles.listitem}
            >
              <Button>Sign Up</Button>
            </List.Item>
          </div>
        </div>
      </List>
    </div>
  );
}

export default Header;
