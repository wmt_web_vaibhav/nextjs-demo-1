import React from "react";
import { Layout, Menu, Breadcrumb } from "antd";

import "../../node_modules/antd/dist/antd.css";
import Header from "./Header";
import "./index.module.scss";
const { Content, Sider } = Layout;

const Layouts = ({ childern }) => {
  return (
    <div>
      <Layout>
        <Header />
        <Content>{childern}</Content>
      </Layout>
    </div>
  );
};

export default Layouts;
