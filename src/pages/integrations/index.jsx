import { SearchOutlined } from "@ant-design/icons";
import { Card, Col, Input, List, Row, Typography } from "antd";
import React, { useState } from "react";
import Layout from "../../components/index";
import comonStyle from "../comon.module.scss";
import integrationsdata from "../../integrationsData.json";
import { useRouter } from "next/router";

function index() {
  const router = useRouter();
  const [integrationData, setIntegrationData] = useState(
    integrationsdata.integrations
  );

  const searchHandleChange = (e) => {
    const integrat = integrationsdata?.integrations?.map((item) => {
      let itemData = item;
      const idata = itemData?.integrations?.filter((data) => {
        if (data.name.includes(e.target.value)) {
          return data;
        }
      });
      itemData.integrations = idata || [];
      return itemData;
    });

    setIntegrationData(integrat);
  };

  return (
    <Layout
      childern={
        <>
          <Row
            style={{
              display: "flex",
              justifyContent: "center",
              margin: "60px 0",
            }}
          >
            <Col xxl={16} xl={16} lg={22} md={22}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "column",
                }}
              >
                <Typography
                  style={{
                    fontSize: "64px",
                    fontWeight: "bold",
                    color: "black",
                  }}
                >
                  Find an Integration
                </Typography>
                <Typography style={{ fontSize: "24px", color: "grey" }}>
                  Extend and automate your workflow by using integrations for
                  your favorite tools.
                </Typography>
              </div>
            </Col>
          </Row>
          <Row style={{ display: "flex", justifyContent: "center" }}>
            <Col xxl={4} xl={4} lg={7} md={7} style={{ paddingRight: "40px" }}>
              <Input
                className={comonStyle.searchInput}
                placeholder="Search Templates..."
                prefix={<SearchOutlined />}
                onChange={searchHandleChange}
              />
              <List style={{ fontSize: "16px", marginTop: "24px" }}>
                <List.Item
                  onClick={() => (window.location.href = "#commerce")}
                  className={comonStyle.listitem}
                >
                  Commerce
                </List.Item>
                <List.Item
                  onClick={() => (window.location.href = "#logging")}
                  className={comonStyle.listitem}
                >
                  Logging
                </List.Item>
                <List.Item
                  onClick={() => (window.location.href = "#databases")}
                  className={comonStyle.listitem}
                >
                  Databases
                </List.Item>
                <List.Item
                  onClick={() => (window.location.href = "#cms")}
                  className={comonStyle.listitem}
                >
                  CMS
                </List.Item>
                <List.Item
                  onClick={() => (window.location.href = "#monitoring")}
                  className={comonStyle.listitem}
                >
                  Monitoring
                </List.Item>
                <List.Item
                  onClick={() => (window.location.href = "#dev-tools")}
                  className={comonStyle.listitem}
                >
                  DevTools
                </List.Item>
                <List.Item
                  onClick={() => (window.location.href = "#messaging")}
                  className={comonStyle.listitem}
                >
                  Messaging
                </List.Item>
              </List>
            </Col>
            <Col xxl={12} xl={12} lg={15} md={15}>
              {integrationData.map((data) => {
                return (
                  <div>
                    <Typography
                      id={data.category.slug}
                      style={{ fontSize: "24px", fontWeight: "bold" }}
                    >
                      {data.category.name}
                    </Typography>
                    <Row
                      style={{
                        display: "flex",
                        justifyContent: "start",
                        padding: "24px 0px",
                      }}
                    >
                      {data?.integrations?.map((item) => {
                        return (
                          <Col
                            xl={8}
                            lg={8}
                            md={12}
                            sm={24}
                            style={{
                              display: "flex",
                              justifyContent: "center",
                              padding: "12px",
                            }}
                          >
                            <Card
                              style={{
                                minHeight: "140px",
                                maxHeight: "320px",
                                borderRadius: "8px",
                                border: "1px solid #eaeaea",
                                cursor: "pointer",
                                borderRadius: "5px",
                              }}
                              className={comonStyle.templateCard}
                              onClick={() =>
                                router.push("/integrations/" + item.slug)
                              }
                              cover={
                                data.category.slug === "featured" &&
                                item?.featuredImages?.[0] ? (
                                  <img
                                    style={{
                                      height: "180px",
                                      borderRadius: "5px 5px 0px 0px",
                                      objectFit: "cover",
                                    }}
                                    alt="example"
                                    src={item.featuredImages[0]}
                                  />
                                ) : (
                                  ""
                                )
                              }
                            >
                              <div
                                style={{
                                  fontSize: "16px",
                                  color: "black",
                                  fontWeight: "bold",
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <img
                                  src={`https://vercel.com/api/www/avatar/${item.icon}?s=88`}
                                  style={{
                                    height: "42px",
                                    width: "42px",
                                    marginRight: "10px",
                                    borderRadius: "50px",
                                  }}
                                />
                                <Typography>{item.name}</Typography>
                              </div>
                              <Typography
                                style={{
                                  fontSize: "14px,",
                                  color: "grey",
                                  marginTop: "16px",
                                }}
                              >
                                {item.shortDescription}
                              </Typography>
                            </Card>
                          </Col>
                        );
                      })}
                    </Row>
                  </div>
                );
              })}
            </Col>
          </Row>
        </>
      }
    />
  );
}

export default index;
