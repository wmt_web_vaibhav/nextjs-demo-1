import React from "react";
import { useRouter } from "next/router";
import Layouts from "../../components";
import { Button, Col, Divider, Row, Typography } from "antd";
import integrationsdata from "../../integrationsData.json";
import comonStyle from "../comon.module.scss";

function integration() {
  const router = useRouter();
  const { integration } = router.query;

  let integrationFind;
  integrationsdata.integrations.map((data) => {
    data.integrations.map((item) => {
      if (item.slug === integration) {
        integrationFind = item;
      }
    });
  });

  return (
    <Layouts
      childern={
        <>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              width: "1048px",
              marginLeft: "auto",
              marginRight: "auto",
            }}
          >
            <Typography
              className={comonStyle.smText}
              style={{ margin: "20px 0px" }}
              onClick={() => {
                router.push("/integrations");
              }}
            >
              <span>&#8592;</span> Back to Integrations Marketplace
            </Typography>
            <Row>
              <Col
                xl={24}
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <div style={{ display: "flex", alignItems: "center" }}>
                  <img
                    style={{ borderRadius: "50px" }}
                    src={`https://vercel.com/api/www/avatar/${integrationFind?.icon}?s=78`}
                  />
                  <Typography
                    style={{
                      fontSize: "54px",
                      padding: "0px",
                      marginLeft: "20px",
                    }}
                  >
                    {integrationFind?.name}
                  </Typography>
                </div>
                <div style={{ display: "flex", alignItems: "center" }}>
                  <Button
                    className="button"
                    style={{ height: "40px", width: "146px" }}
                  >
                    Add Integration
                  </Button>
                  <Button
                    className="button1"
                    style={{ height: "40px", width: "146px" }}
                  >
                    Select a Template
                  </Button>
                </div>
              </Col>
            </Row>
          </div>
          <Divider />
          <div
            style={{
              overflowX: "scroll",
              padding: "0px 0px 20px 0px",
            }}
          >
            <div
              style={{
                width: `calc(${
                  (integrationFind?.featuredImages?.length || 0) * 504
                }px + 100vw - 1048px + 24px)`,
                maxWidth: "none",
                display: "flex",
                justifyContent: "center",
              }}
            >
              {integrationFind?.featuredImages.map((item) => {
                return (
                  <div
                    style={{
                      margin: "0px 12px",
                      scrollSnapAlign: "none",
                      padding: "60px 0px",
                    }}
                  >
                    <img
                      src={item}
                      style={{
                        width: "480px",
                        height: "320px",
                        objectFit: "cover",
                        borderRadius: "8px",
                      }}
                    />
                  </div>
                );
              })}
            </div>
          </div>
        </>
      }
    />
  );
}

export default integration;
