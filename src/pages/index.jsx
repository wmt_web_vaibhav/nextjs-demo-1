import Head from "next/head";
import { useRouter } from "next/router";
import Layout from "../components/index";
import { Button, Col, Row, Typography } from "antd";
import styles from "./index.module.scss";

function first() {
  return (
    <Layout
      childern={
        <>
          <Row
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Col
              xl={14}
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                padding: "3.5rem 0px",
                textAlign: "center",
              }}
              className={styles.homeparent}
            >
              <Typography className={styles.home1}>Develop.</Typography>
              <Typography className={styles.home2}>Preview.</Typography>
              <Typography className={styles.home3}>Ship.</Typography>
              <div style={{ marginTop: "50px" }}>
                <Button className={styles.button}>Start Deploying</Button>
                <Button className={styles.button1}>Get a Demo</Button>
              </div>
              <Typography
                style={{ fontSize: "20px", margin: "50px 0px", color: "grey" }}
              >
                Vercel combines the best developer experience with an obsessive
                focus on end-user performance. Our platform enables frontend
                teams to do their best work.
              </Typography>
            </Col>
          </Row>
        </>
      }
    />
  );
}

export default first;
