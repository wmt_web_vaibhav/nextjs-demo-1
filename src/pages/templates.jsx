import { CodepenCircleOutlined, SearchOutlined } from "@ant-design/icons";
import { Avatar, Card, Col, Input, Row, Typography } from "antd";
import React, { useState } from "react";
import Layout from "../components/index";
import comonStyle from "./comon.module.scss";

import tamplatedata from "../templatesData.json";
import { useRouter } from "next/router";

function templates() {
  const [templatesData, setTemplatesData] = useState(tamplatedata.templates);

  const searchHandleChange = (e) => {
    const templet = tamplatedata?.templates?.filter(
      (item) =>
        item.name.includes(e.target.value) ||
        item.description.includes(e.target.value)
    );
    setTemplatesData(templet);
  };

  const router = useRouter();
  return (
    <Layout
      childern={
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "1048px",
            marginLeft: "auto",
            marginRight: "auto",
            padding: "0px 24px",
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              padding: "3.5rem 0 0 0",
            }}
          >
            <Typography
              style={{
                fontSize: "40px",
                fontWeight: "bold",
                color: "black",
              }}
            >
              Select a template.
            </Typography>
            <div>
              <Input
                style={{ width: "318px" }}
                className={comonStyle.searchInput}
                placeholder="Search Templates..."
                prefix={<SearchOutlined />}
                onChange={searchHandleChange}
              />
            </div>
          </div>
          <Typography style={{ color: "grey", margin: "17px 0" }}>
            We support all your favorite frameworks without additional
            configuration.
          </Typography>
          <Row style={{ display: "flex", justifyContent: "center" }}>
            <Col>
              <Row
                style={{
                  display: "flex",
                  padding: "24px 0px",
                }}
              >
                {templatesData?.map((data) => {
                  return (
                    <Col
                      xl={8}
                      style={{
                        // display: "flex",
                        // justifyContent: "center",
                        padding: "12px",
                      }}
                    >
                      <Card
                        style={{
                          width: "318px",
                          minHeight: "122px",
                          maxHeight: "287px",
                          borderRadius: "8px",
                          //   border: "1px solid #eaeaea",
                          cursor: "pointer",
                        }}
                        className={comonStyle.templateCard}
                        onClick={() => router.push(data.website)}
                      >
                        {data?.screenshot ? (
                          <img
                            style={{
                              height: "159px",
                              width: "262px",
                              borderRadius: "4px",
                              marginBottom: "24px",
                              objectFit: "cover",
                            }}
                            alt="example"
                            src={data.screenshot}
                          />
                        ) : null}

                        <div
                          style={{
                            fontSize: "16px",
                            color: "black",
                            fontWeight: "bold",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <img
                            src={data.logo}
                            style={{
                              height: "20px",
                              width: "20px",
                              marginRight: "10px",
                            }}
                          />
                          <Typography>{data.name}</Typography>
                        </div>
                        <Typography
                          style={{ fontSize: "14px,", color: "grey" }}
                        >
                          {data.description}
                        </Typography>
                      </Card>
                    </Col>
                  );
                })}
              </Row>
            </Col>
          </Row>
        </div>
      }
    />
  );
}

export default templates;
